from django.contrib import admin

from .models import Project

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'user', 'type', 'shared', 'likes', 'reinventions')
    ordering = ('name',)
    search_fields = ( 'name', 'description')
