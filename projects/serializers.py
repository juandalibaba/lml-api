import json
from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Project, Profile


def extractClasses(project):
    json_data_dict = project.json_data['data']

    classes = []

    for _class in json_data_dict:
        classes.append(_class)

    return classes


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email']


class ProjectSerializer(serializers.ModelSerializer):
    json_data = serializers.JSONField(binary=True, required=False)
    username = serializers.SerializerMethodField('get_username')
    classes = serializers.SerializerMethodField('extractClasses')

    def get_username(self, project):
        return project.user.username

    def extractClasses(self, project):
        return extractClasses(project)

    class Meta:
        model = Project
        fields = ['id', 'name', 'description', 'json_data',
                  'uuid', 'user', 'classes', 'username', 'shared', 'reinventions', 'type']


class ProjectMetaDataSerializer(serializers.ModelSerializer):

    classes = serializers.SerializerMethodField('extractClasses')

    def extractClasses(self, project):
        return extractClasses(project)

    class Meta:
        model = Project
        fields = ['id', 'name', 'description', 'uuid',
                  'user', 'classes', 'likes', 'shared', 'reinventions', 'type']