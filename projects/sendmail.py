import uuid
from datetime import datetime

from django.core.mail import EmailMultiAlternatives, send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings


subject, from_email = 'Bienvenido a LearningML', 'no-reply@learningml.org'

def generate_token_signup(user):
    uid = str(uuid.uuid1())
    user.profile.activation_token = uid
    user.save()

    return uid


def greater_than_14(user):
    currentMonth = datetime.now().month
    currentYear = datetime.now().year

    deltaYear = currentYear - int(user.profile.year)

    return deltaYear >= 14


def send_signup_email(user):
    confirmation_token = generate_token_signup(user)
    url_confirmation = '{}/projects/activate/{}'.format(settings.URL_BASE, confirmation_token)
    template = 'signup_mail_template.html' \
        if greater_than_14(user) \
        else 'signup_for_parent_mail_template.html'
    html_content = render_to_string(template,
                                    {'username': user.username, 'url_confirmation': url_confirmation })
    text_content = strip_tags(html_content)
    # create the email, and attach the HTML version as well.
    msg = EmailMultiAlternatives(subject, text_content, from_email, [user.email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def send_bug_email(user, title, message):
    subject = title
    content = "Usuario: {}, Mensaje: {}".format(user, message)
    send_mail(
        subject,
        content,
        from_email,
        [settings.EMAIL_TICKET_GITLAB],
        fail_silently=False,
    )

def send_prueba_email():
    subject = "Prueba LML email sendgrid"
    content = "Esto es una prueba"
    send_mail(
        subject,
        content,
        from_email,
        ["juanda@juandarodriguez.es"],
        fail_silently=False,
    )
