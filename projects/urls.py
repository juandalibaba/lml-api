from django.urls import path
from . import views


urlpatterns = [
    path('', views.ApiProjectListView.as_view(), name='projects'),
    path('<int:pk>', views.ApiProjectDetailView.as_view()),
    path('signup', views.ApiSignupView.as_view()),
    path('sendbug', views.ApiSendBugView.as_view()),
    path('checkusername/<str:username>', views.ApiUserView.as_view()),
    path('activate/<str:activation_token>', views.SignupView.as_view()),
    path('operation/<int:pk>/<str:operation>', views.ApiProjectCommunityOperations.as_view())
]