# LearningML API y cuentas usuario


## Para desarrollar

Crear entorno virtual con el fichero requirements.txt.

    pip install requirements.txt
   
Arrancar el servidor de desarrollo

    ./manage.py runserver
    
Cuando se añaden nuevos elementos al modelo hay que realizar las migraciones

    ./manage.py makemigrations
    ./manage.py migrate
    

## Rutas API

### Obtención del token

`POST /api-token-auth`

body

`{"username": "ciro", "password": "1dcnnqa2"}`

### Registro

`POST /projects/signup`

body

`{"username": "ciro", "password": "1dcnnqa2", "email": "ciro@kk.es", "month": "5", "year": "2014", "gender": "M"}`

### Comprobar si existe un username

`GET /projects/username/[username]`

### Obtener un proyecto

`GET /projects/{id}`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

### Obtener todos los proyectos de un usuario

`GET /projects`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`


### Obtener todos los proyectos compartidos

`GET /projects?shared=1`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

### Enviar un nuevo proyecto

`POST /projects`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

body

`{"name":"el proyectongazo ","description":"isisisisisissdsada", "shared":"0", "json_data":"{\"lilu\":\"lala\", \"klkl\":\"jdd\"}"}`

### Modificar un proyecto

`PUT /projects/{id}`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

body

`{"name":"el proyectongazo modificado","description":"isisisisisissdsada","json_data":"{\"lilu\":\"lala\", \"klkl\":\"jdd\"}"}`

## Borrar un proyecto

`DELETE /projects/{id}`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

## Añadir una reinvención a un proyecto

`PUT /projects/operation/{id}/{operation}`

operation puede ser

 - reinvent, suma 1 a reinventions
 - like, suma 1 a likes

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`


## Enviar un ticket a gitlab

`POST /projects/sendbug`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`


# Servicio de correo electrónico

Para el envío de los emails de bienvenida y activación de la cuenta
estoy usando sendgrid. 

 - https://app.sendgrid.com/
 
En el archivo ``settings.py`` se encuentra la configuración del servicio.

### GPL License

LeaningML, the easiest way to learn Machine Learning foundamentals

Copyright (C) 2020 Juan David Rodríguez García & KGBLIII

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.